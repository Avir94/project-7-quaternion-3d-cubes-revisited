#ifndef QUATRBT_H
#define QUATRBT_H

#include <iostream>
#include <cassert>

#include "matrix4.h"
#include "quat.h"

class QuatRBT {
  Cvec3 t_; // translation component
  Quat r_;  // rotation component represented as a quaternion

public:
  QuatRBT() : t_(0) {
    assert(norm2(Quat(1,0,0,0) - r_) < CS150_EPS2);
  }

  QuatRBT(const Cvec3& t, const Quat& r) {
    t_ = t;
    r_ = r;
  }

  explicit QuatRBT(const Cvec3& t) {
    t_ = t;
    r_ = Quat();
  }

  explicit QuatRBT(const Quat& r) {
    r_ = r;
    t_ = Cvec3();
  }

  Cvec3 getTranslation() const {
    return t_;
  }

  Quat getRotation() const {
    return r_;
  }

  QuatRBT& setTranslation(const Cvec3& t) {
    t_ = t;
    return *this;
  }

  QuatRBT& setRotation(const Quat& r) {
    r_ = r;
    return *this;
  }

  Cvec4 operator * (const Cvec4& a) const {
    const Cvec4 r = r_ * a + Cvec4(0, t_[0], t_[1], t_[2]);
    return Cvec4(r[1], r[2], r[3], a[3]);
  }

  QuatRBT operator * (const QuatRBT& a) const {
    Cvec3 at = Cvec3(a.getTranslation());
    Quat r = Quat(a.getRotation());
    Quat t = Quat(0, at[0], at[1], at[2]);
    t = r_ * t * inv(r_) + Quat(0, t_[0], t_[1], t_[2]);
    return QuatRBT(Cvec3(t[1], t[2], t[3]), Quat(r_ * r));
  }
};

inline QuatRBT inv(const QuatRBT& tform) {
  Quat r = tform.getRotation();
  Cvec3 t = tform.getTranslation();
  return QuatRBT(Cvec3(inv(r)*Cvec4(t)) * (-1.0), inv(r));
}

inline QuatRBT transFact(const QuatRBT& tform) {
  return QuatRBT(tform.getTranslation());
}

inline QuatRBT linFact(const QuatRBT& tform) {
  return QuatRBT(tform.getRotation());
}

inline Matrix4 rigTFormToMatrix(const QuatRBT& tform) {
  Quat r = tform.getRotation();
  Matrix4 m = quatToMatrix(r);
  Cvec3 t = tform.getTranslation();
  for(int i=0; i<3; i++){
    m(i,3) = t[i];
  }
  return m;
}

#endif
